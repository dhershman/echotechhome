# EchoTech Home #

Simple Portfolio web page built on Node.js using the Hapi.js framework and webpack modular build tool.

Current Features:

* Home
* About
* Example Designs

Future Features:

* Built in Demo Environment to demo apps
* Project/Future Project List 