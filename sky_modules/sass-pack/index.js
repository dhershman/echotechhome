#! /usr/bin/env node

'use strict';

const fs = require('fs');
const sass = require('node-sass');
const path = require('path');
const debug = require('debug')('sky:sass-pack');
const program = require('commander');
const progressBar = require('progress');

program
	.version('1.0.0')
	.option('-s, --sourcepath [tpath]', 'Add a source path')
	.option('-t, --themepath [tpath]', 'Add a theme path')
	.option('-m, --manifestpath [mpath]', 'Add a manifest path')
	.option('-o, --output [opath]', 'Add the output path')
	.parse(process.argv);

sassPack(program);

function sassPack(opts) {
	let manifest = JSON.parse(fs.readFileSync(opts.manifestpath, 'utf8'));
	buildSass();

	function getThemeSass() {
		let themeDir = fs.readdirSync(opts.themepath);
		let themeSass = [];
		let i = 0;
		let len = themeDir.length;
		for (i; i < len; i++) {
			let tmp = fs.readdirSync(path.join(opts.themepath, themeDir[i]));
			for (let x = 0; x < tmp.length; x++) {
				if (tmp[x].indexOf('-styles') > -1) {
					themeSass.push(path.join(opts.themepath, themeDir[i], tmp[x]));
				}
			}
		}
		return themeSass;
	}

	function getPageSass(loc, data) {
		let srcPath = loc || opts.sourcepath;
		let srcFiles = fs.readdirSync(srcPath);
		let pageSass = data || {
			themes: getThemeSass(),
			pages: []
		};
		let i = 0;
		let len = srcFiles.length;
		for (i; i < len; i++) {
			if (srcFiles[i].indexOf('_components') > -1 || srcFiles[i].indexOf('_client') > -1) continue;
			if (fs.statSync(path.join(srcPath, srcFiles[i])).isDirectory()) getPageSass(path.join(srcPath, srcFiles[i]), pageSass);

			if (srcFiles[i].indexOf('.scss') > -1) {
				if (!fs.readFileSync(path.join(srcPath, srcFiles[i]), 'utf8')) continue;
				pageSass.pages.push(path.join(srcPath, srcFiles[i]));
			}
		}
		return pageSass;
	}

	function buildSass() {
		let scssObj = getPageSass();
		let scss = '';
		let name = getThemeName(scssObj.themes);
		debug('Building Theme - ' + name);
		var prop;

		for (prop in scssObj) {
			let i = 0;
			let len = scssObj[prop].length;
			let bar = new progressBar('Compiling SASS [:bar] :current/:total :elapsed :percent', {
				total: len,
				width: 20
			});
			for (i; i < len; i++) {
				bar.tick();
				scss += '\n' + fs.readFileSync(scssObj[prop][i], 'utf8');
			}
		}
		compile(name, scss);
	}

	function getThemeName(paths) {
		let len = paths.length;
		let i = 0;
		for (i; i < len; i++) {
			if (paths[i].split('\\').indexOf('themes') > -1) {
				let tmp = paths[i].split('\\');
				return tmp[tmp.indexOf('themes') + 1];
			}
		}
	}

	function compile(name, css) {
		sass.render({
			data: css
		}, (err, result) => {
			if (err) return debug(err);
			fs.writeFile(path.resolve(opts.output, name + '.css'), result.css, (err) => {
				if (err) throw err;
				debug('Compiled Main Theme -- Done');
				writeManifest(path.resolve(opts.output, name + '.css'), name);
			});
		});
	}

	function writeManifest(path, name) {
		if (!manifest.hasOwnProperty(name)) {
			manifest[name] = '/assets/css/' + name;
			fs.writeFileSync(opts.manifestpath, JSON.stringify(manifest), 'utf8');
		}
	}
}

module.exports = sassPack;
