**Sass-Pack**
=============

A simple CLI system for compiling sass down to CSS and builds a css_manifest for webpack

Command & Options
--------

 - `sass-pack [options]` - Run sass-pack using your options
 - `-s`, `--sourcepath` - Set the source path of page based scss
 - `-t`, `--themepath` - Set the path to your `theme` scss
 - `-m`, `--manifestpath` - Set the path of your `css_manifest.json`
 - `-o`, `--output` - Set the path for the output css
 - `-v`, `--version` - Display the module version

> sass-pack -o public/css -s src/app -t public/scss/themes -m src/config/css_manifest.json

Requirements
------------

 - `node-sass` - the node scss compiler library
 - `commander` - module to setup cli support


Optional
--------

 - `debug` - display messages into the console

Future TODO
-----------
Make the App more crawl dynamic to avoid pathing options in cli & applying a better dynamic approach.