'use strict';

module.exports = (server) => {
	server.ext('onPreResponse', (request, reply) => {
		//Check for some booms
		if (request.response.isBoom) {
			let statusCode = request.response.output.payload.statusCode;
			if (statusCode >= 500) {
				recordError(request);
			}
			//Temporary solution
			if (request.path === '/projects') {
				var percent = '0';
				switch (request.path) {
					case '/projects':
						percent = '20';
						break;
					default:
						percent = '0';
						break;
				}
				reply.view('templates/construction', {
					page: {
						title: request.path.replace('/', '') + ' construction',
						progress: percent
					},
					css: {
						theme: 'default'
					}
				}).code(statusCode);
			} else {
				reply.view('templates/404', {
					css: {
						theme: 'default'
					}
				}).code(statusCode);
			}

		} else {
			reply.continue();
		}
	});

	//This will be something in the future I swear.
	function recordError(request) {
		let errOutput = {
			error: request.response.output,
			stack: request.response.stack,
			request: {
				id: request.id,
				instance: request.connection.info.uri,
				labels: request.connection.settings.labels,
				path: request.url.path,
				query: request.query,
				method: request.method,
				payload: request.payload,
				headers: request.headers,
				responseTime: request.info.responded - request.info.received,
				info: request.info
			}
		};
		return errOutput;
	}

	return server;
};
