'use strict';

const Boom = require('boom');

module.exports = (server) => {
	server.method('preExists', (request, reply) => {
		const resolutions = [
			value => {
				try {
					return require.resolve(request.route.realm.plugin + '/' + request.paramsArray.join('/'));
				} catch (e) {
					return null;
				}
			},

			value => {
				try {
					return require.resolve('../app' + value);
				} catch (e) {
					return null;
				}
			}
		];

		for(const resolver of resolutions){
			let handler = resolver(request.path), notFound = (handler == null);
			if (!notFound)
				return reply(handler);
		}
		return reply(Boom.notFound('Not Found'));
	});

	server.method('preAssets', (request, reply) => {
		let formatpath = request.path.replace('/', '').replace(/[/]/g, '-'); //remove first one and then replace the rest.
		//CSS is temporarily blank until I get a good theme system in place to populate it properly,
		//I still want webpack to be able to handle it however so we will leave the
		//system in place until we can get a good theme system down
		let theme = (request.auth.artifacts) ? request.auth.artifacts.session.theme : 'default';
		return reply({
			js: {
				page: request.server.app.jsManifest[`${formatpath}`].js,
				manifest: request.server.app.jsManifest.manifest.js,
				vendor: request.server.app.jsManifest.vendor.js
			},
			css: {
				theme: request.server.app.cssManifest[theme]
			}
		});
	});

	server.method('preUser', (request, reply) => {
		//Temporary solution until I find a better theme system maybe like leonard in v6 me thinks.
			return reply({
				theme: (request.auth.artifacts) ? request.auth.artifacts.session.theme : 'default'
			});
	});

	return server;
};