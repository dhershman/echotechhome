'use strict';

const path = require('path');

module.exports = (server) => {

	server.route({
		path: '/',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return reply.redirect('/home');
		}
	});

	server.route({
		method: 'GET',
		path: '/assets/{param*}',
		handler: {
			directory: {
				path: path.resolve('./public')
			}
		}
	});

	server.route({
		path: '/{path*}',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return require(request.pre.handler)(request, reply);
		},
		config: {
			pre:
			[
				{ method: 'preExists', assign: 'handler' },
				[
					// executed in parallel, outside array is sync.
					{ method: 'preAssets', assign: 'assets' },
					{ method: 'preUser', assign: 'user' }
				]
			]
		}
	});

	return server;

};
