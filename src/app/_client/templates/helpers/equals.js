'use strict';

module.exports = function(context, string, options) {
	if (context !== null && context.toString().length && context === string) {
		return options.fn(this);
	}
	return options.inverse(this);
};