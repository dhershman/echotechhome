'use strict';

function designsMain($el, opts) {

	$el.on('click', '#switchGallery', handler)
		.on('click', '#switchExamples', handler);

	function handler(e) {
		var page = $(e.currentTarget).attr('id');
		return render({}, page.replace('switch', '').toLowerCase());
	}

	function render(d, page) {
		$el.fadeOut('fast', function fading() {
			$(this).html(opts.templates[page](d)).fadeIn('fast');
			opts.modalHandler();
		});
	}

	//Responsive design
	$(window).resize(function respond() {
		if ($(this).width() <= 980) {
			$('#navMenu').addClass('hidden');
		} else {
			$('#navMenu').removeClass('hidden');
		}
	});

	return {
		render: render
	};
}

module.exports = designsMain;
