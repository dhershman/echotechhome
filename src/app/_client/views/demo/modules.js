'use strict';

function moduleViews($el, opts) {

	var mockData = [{
		name: 'Dustin',
		level: 16,
		joinDate: '09/16/2015'
	}, {
		name: 'Brandon',
		level: 18,
		joinDate: '08/15/2015'
	}, {
		name: 'Will',
		level: 20,
		joinDate: '10/22/2014'
	}, {
		name: 'Jessie',
		level: 55,
		joinDate: '01/09/2013'
	}, {
		name: 'Ben',
		level: 3,
		joinDate: '07/10/2016'
	}, {
		name: 'Sean',
		level: 32,
		joinDate: '05/12/2014'
	}, {
		name: 'Phil',
		level: 1,
		joinDate: '11/14/2016'
	}, {
		name: 'Cyndee',
		level: 15,
		joinDate: '11/08/2016'
	}, {
		name: 'Kevin',
		level: 10,
		joinDate: '09/18/2016'
	}];

	var filterhandler = opts.modules.filterhandler({
		searchableData: mockData
	});

	function render(d) {
		$el.find('#appDemo').html(opts.template({ data: d, filterhandler: true }));
		$el.removeClass('hidden');
	}

	$el.on('click', '.sort', function sortItem() {
		var updatedData = filterhandler.sortBy($(this).data('type'), $el.find('#descendingCheck').is(':checked'));
		render(updatedData);
	}).on('click', '#searchBtn', function searchItem(e) {
		e.preventDefault();
		var updatedData = filterhandler.search($el.find('#searchBox').val());
		render(updatedData);
	});

	render(mockData);

}

module.exports = moduleViews;
