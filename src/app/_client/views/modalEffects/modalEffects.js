module.exports = function() {
	var $page = $('main');
	function init() {
		var $overlay = $page.find('.md-overlay');
		var triggers = $page.find('.md-trigger');
		_.forEach(triggers, function(el) {
			var $el = $(el);
			var $modal = $page.find( '#' + $el.data( 'modal' ) ),
				$close = $modal.find( '.md-close' );

			function removeModal( hasPerspective ) {
				$modal.removeClass('md-show');

				if( hasPerspective ) {
					$page.removeClass('md-perspective');
				}
			}

			function removeModalHandler() {
				removeModal( $el.hasClass('md-setperspective'));
			}

			$el.on( 'click', function() {
				$page.find( '#' + $el.data( 'modal' ) ).addClass('md-show');

				//Until a better way is found... this must be done. Blegh
				setTimeout(function() {
					$page.one('click', $overlay, removeModalHandler );
				}, 15);
				if( $el.hasClass('md-setperspective') ) {
					setTimeout( function() {
						$page.addClass('md-perspective');
					}, 25 );
				}
			});

			$modal.on('click', $close, function(e) {
				e.stopPropagation();
				removeModalHandler();
			});

		} );

	}

	init();
};
