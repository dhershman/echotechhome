'use strict';

var getParams = require('sky_modules/getParameterByName');

//Templates
var templates = {
	module: require('templates/demo/modules.hbs')
};

//Modules
var modulesView = require('client/views/demo/modules');
var modules = {
	filterhandler: require('sky_modules/filterhandler')
};

$(function() {
	function demoController() {
		var $page = $('#demoContent');
		var app = getParams('app');

		switch(app) {
			case 'modules':
				modulesView($page.find('#appContent'), {
					template: templates.module,
					modules: modules
				});
				break;
			default:
				break;
		}
	}

	demoController();
});