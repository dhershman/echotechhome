'use strict';

module.exports = (request, reply) => {
	let pageOpts = {
		page: {
			body: 'demo',
			title: 'Demos',
			commons: request.server.app.jsManifest['commons.client'].js
		}
	};
	reply.view('app/demo/index', Object.assign(pageOpts, request.pre.assets, request.pre.user));
};
