'use strict';

var path		= require('path');
var webpack		= require('webpack');

module.exports = (config) => {

	var chunks = config._buildEntries(__dirname);

	config.resolve.alias['client'] = path.resolve(__dirname, '_client');

	config.plugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			name: 'commons.client',
			minChunks: 3,
			chunks: chunks
		})
	);

	return config;
};
