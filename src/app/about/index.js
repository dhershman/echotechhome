'use strict';

module.exports = (request, reply) => {
	let pageOpts = {
		page: {
			body: 'about',
			title: 'About',
			commons: request.server.app.jsManifest['commons.client'].js
		}
	};
	reply.view('app/about/index', Object.assign(pageOpts, request.pre.assets, request.pre.user));
};
