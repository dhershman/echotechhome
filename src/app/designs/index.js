'use strict';

module.exports = (request, reply) => {
	let pageOpts = {
		page: {
			body: 'designs',
			title: 'Design Examples',
			commons: request.server.app.jsManifest['commons.client'].js
		}
	};
	reply.view('app/designs/index', Object.assign(pageOpts, request.pre.assets, request.pre.user));
};
