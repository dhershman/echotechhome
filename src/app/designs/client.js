'use strict';
var modalHandler = require('client/views/modalEffects/modalEffects');

var designsView = require('client/views/designs/main');


var templates = {
	gallery: require('templates/designs/gallery.hbs'),
	examples: require('templates/designs/examples.hbs')
};

$(function() {
	function designsController() {
		var $page = $('#designs');
		var mainView = designsView($page.find('#designsMain'), {
			templates: templates,
			modalHandler: modalHandler
		});
		mainView.render({}, 'examples');

	}

	designsController();
});
