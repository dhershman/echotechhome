'use strict';

module.exports = (request, reply) => {
	let pageOpts = {
		page: {
			body: 'home',
			title: 'Home',
			commons: request.server.app.jsManifest['commons.client'].js
		}
	};
	reply.view('app/home/index', Object.assign(pageOpts, request.pre.assets, request.pre.user));
};
