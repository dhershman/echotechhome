/**
 * @fileOverview require templates on demand for webpack.
 * @author Sean Grasso
 * @since  2016-09-20
 */

'use strict';

module.exports = function(templateName, cb) {
	require.ensure([
		'client/templates/helpers/equals.js'
	], function (require) {
		console.log(templateName);
		require('bundle?lazy!client/templates' + templateName)(cb);
	});
};